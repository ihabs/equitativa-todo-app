-Description: todo-app is a simple real estate projects progress tracking web app, allows the user to manage and view projects per building and per person.

-Tech Stack:
Java version 1.8.0_171
Apache Wicket 8.2.0
Jetty 9.4 
Bootstrap v4.1.3
PostgreSQL

-Features:
Easy to use interface 
Search filters allows the user to view specific assignee or/and building todos  
Informative Tutorial helps the user learn how get started with the App  

-Challenges
learning Apache Wicket
providing a simple and easy to use interface 



