package com.equitativa.persistence.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.equitativa.persistence.model.Todo;

public class TestTodoDao {
	
	
	private TodoDao todoDao;
	private Long dummyID;
	private String title;

	@Before
	public void setUp()
	{
		todoDao = new TodoDao();
		 dummyID = 1001l;
		 title = "cleaning the facade";
	}

	@Test
	public void insertAndFindEntitySuccessfully()
	{
		Todo todo = new Todo(dummyID,title, "cleaning the facade and move all uneeded blocks");
		todoDao.save(todo);
		Todo todoEntity = todoDao.findById(dummyID);
		assertEquals(todoEntity.getTitle(), title);
		todoDao.delete(todo);
	}
	
	@Test
	public void updateEntitySuccessfully()
	{
		Todo todo = new Todo(dummyID,title, "cleaning the facade and move all uneeded blocks");
		todoDao.save(todo);
		todo.setTitle(title+".");
		todoDao.update(todo);
		Todo todoEntity = todoDao.findById(dummyID);
		assertEquals(todoEntity.getTitle(), title+".");
		todoDao.delete(todo);
	}
	


}
