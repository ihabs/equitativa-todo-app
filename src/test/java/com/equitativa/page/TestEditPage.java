package com.equitativa.page;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;

import com.equitativa.TodoApplication;

/**
 * Simple test using the WicketTester
 */
public class TestEditPage
{
	private WicketTester tester;

	@Before
	public void setUp()
	{
		tester = new WicketTester(new TodoApplication());
	}

	@Test
	public void todoPageRendersSuccessfully()
	{
		//start and render the test page
		tester.startPage(EditPage.class);

		//assert rendered page class
		tester.assertRenderedPage(EditPage.class);
	}
}
