package com.equitativa.page;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.PackageResourceReference;
/**
 * 
 * HomePage building home page/components 
 *
 */
public class HomePage extends WebPage {
	private static final String APP_DESCRPTION = "Todo-app is a simple real estate projects progress tracking web app, allows the user to manage and view projects per building and per person.";
	private static final long serialVersionUID = 1L;
	
	private static Integer imgIndex = 0;

	public HomePage(final PageParameters parameters) {
		super(parameters);

		add(new Label("descrption", APP_DESCRPTION));
		
		
		Image image = new Image("img", new PackageResourceReference(HomePage.class, "help/images/"+imgIndex+".png"));
		
		add(image);

		AjaxLink<String> nextButton = new AjaxLink<String>("nxt") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				if(imgIndex < 3) {
					imgIndex++;
				}
				setResponsePage(HomePage.class , parameters);
			}
			
		};
		add(nextButton);
		
		AjaxLink<String> prevButton = new AjaxLink<String>("prv") {
			private static final long serialVersionUID = 1L;
			@Override
			public void onClick(AjaxRequestTarget target) {
				if(imgIndex > 0) {
					imgIndex--;
				}
				setResponsePage(HomePage.class , parameters);
			}
			
		};
		add(prevButton);
		
		
	}
}
