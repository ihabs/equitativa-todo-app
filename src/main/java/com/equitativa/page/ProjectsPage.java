package com.equitativa.page;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.equitativa.common.Config;
import com.equitativa.data.provider.TodoDataProvider;
import com.equitativa.page.components.EditModal;
import com.equitativa.page.components.FilterForm;
import com.equitativa.page.components.TodoDataView;

/**
 * 
 * ProjectsPage building the projects page/components
 *
 */
public class ProjectsPage extends WebPage {
	private static final String ADD_BUTTON = "add";

	private static final String FILTER_FORM = "filterForm";

	private static final String NO_PROJECTS_MSG = "No projects found, click the add button to start creating projects.";

	private static final String NAVIGATOR = "navigator";

	private static final String PAGEABLE = "pageable";

	private static final String EDIT_MODAL = "editModal";

	private static final long serialVersionUID = 1L;

	public ProjectsPage(PageParameters pageParameters) {

		// add the edit window
		EditModal editModal = new EditModal(EDIT_MODAL, pageParameters);
		editModal.setWindowClosedCallback(editModal);
		add(editModal);

		// add the assignee and building filter
		Form<String> filterForm = new FilterForm(FILTER_FORM, pageParameters);
		add(filterForm);

		// add main todo list table
		TodoDataProvider todoDataProvider = new TodoDataProvider(pageParameters.get(Config.Entity.Assignee.name().toLowerCase()), pageParameters.get(Config.Entity.Building.name().toLowerCase()));
		TodoDataView todoDataView = new TodoDataView(PAGEABLE, todoDataProvider, editModal, pageParameters);
		add(todoDataView);

		// add the table navigator/message based on the data size
		if (todoDataProvider.size() == 0) {
			add(new Label(NAVIGATOR, NO_PROJECTS_MSG));
		} else if (todoDataProvider.size() <= 8) {
			add(new Label(NAVIGATOR, ""));
		} else {
			add(new PagingNavigator(NAVIGATOR, todoDataView));
		}
		// add a button to add new item
		AjaxLink<Void> addButton = new AjaxLink<Void>(ADD_BUTTON) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				editModal.setPageCreator(() -> new EditPage(pageParameters, editModal));
				editModal.show(target);
			}
		};
		add(addButton);

	}

}
