package com.equitativa.page;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.equitativa.page.components.EditForm;
import com.equitativa.persistence.model.Todo;

/**
 * 
 * EditPage building the todo page/components
 *
 */
public class EditPage extends WebPage {
	private static final String EDIT_FORM = "editForm";
	private static final String EDIT_LABEL = "editLabel";
	private static final long serialVersionUID = 1L;

	public EditPage(PageParameters pageParameters, ModalWindow modal) {
		this(pageParameters,null, modal);
	}
	public EditPage(PageParameters pageParameters, Todo todo, ModalWindow modal) {

		if (todo != null) {
			// edit item
			add(new Label(EDIT_LABEL, String.valueOf("Edit project")));
		} else {
			// add item
			add(new Label(EDIT_LABEL, String.valueOf("Add project")));
		}
		add(new EditForm(EDIT_FORM, todo, modal));

	}

}
