package com.equitativa.page.components;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.IRequestParameters;

import com.equitativa.common.Config;
import com.equitativa.persistence.dao.AssigneeDao;
import com.equitativa.persistence.dao.BuildingDao;
import com.equitativa.persistence.dao.TodoDao;
import com.equitativa.persistence.model.Assignee;
import com.equitativa.persistence.model.Building;
import com.equitativa.persistence.model.Todo;
/**
 * 
 * EditForm used edit/create todo 
 *
 */
public class EditForm extends Form<Todo> {

	private static final String SAVE = "save";
	private static final long serialVersionUID = 1L;

	public EditForm(String id, Todo todo, ModalWindow modal) {
		super(id);
	    
	    if (todo != null) {
			// edit item
			add(new HiddenField<>(Config.TodoFields.ID.name(), Model.of(todo.getId())));
			add(new TextField<>(Config.TodoFields.Title.name(), Model.of(todo.getTitle())));
			add(new TextField<>(Config.TodoFields.Description.name(), Model.of(todo.getDescription())));
		} else {
			// add item
			add(new HiddenField<>(Config.TodoFields.ID.name(), Model.of("")));
			add(new TextField<>(Config.TodoFields.Title.name(), Model.of("")));
			add(new TextField<>(Config.TodoFields.Description.name(), Model.of("")));
		}
	    //assignees drop down
	    IModel<List<Assignee>> assignees = () -> new AssigneeDao().findAll();
		final DropDownChoice<Assignee> assignee = new DropDownChoice<>(Config.Entity.Assignee.name(), new Model<>(), assignees, new ChoiceRenderer<Assignee>() {

			private static final long serialVersionUID = 1L;

			public Object getDisplayValue(Assignee object) {
				return object.getName();
			};

			public String getIdValue(Assignee object, int index) {
				return String.valueOf(object.getId());
			};

		});
		assignee.setNullValid(false);
		assignee.setDefaultModelObject(assignees.getObject().get(0));
		//buildings drop down
		IModel<List<Building>> buildings = () -> new BuildingDao().findAll();

		final DropDownChoice<Building> building = new DropDownChoice<>(Config.Entity.Building.name(), new Model<>(), buildings, new ChoiceRenderer<Building>() {

			private static final long serialVersionUID = 1L;

			public Object getDisplayValue(Building object) {
				return object.getName();
			};

			public String getIdValue(Building object, int index) {
				return String.valueOf(object.getId());
			};

		});

		building.setNullValid(false);
		building.setDefaultModelObject(buildings.getObject().get(0));
		
		if (todo != null) {
			for (Building obj : buildings.getObject()) {
				if (obj.getId() == todo.getBuilding().getId()) {
					building.setDefaultModelObject(obj);
					break;
				}
			}
			for (Assignee obj : assignees.getObject()) {
				if (obj.getId() == todo.getAssignee().getId()) {
					assignee.setDefaultModelObject(obj);
					break;
				}
			}
		}
		
		add(assignee);
		add(building);

		add(new AjaxButton(SAVE) {
			private static final long serialVersionUID = 1L;

			protected void onSubmit(AjaxRequestTarget target) {
				modal.close(target);
			};
		});
	}

	@Override
	protected void onSubmit() {
		TodoDao todoDao = new TodoDao();
		IRequestParameters postParameters = getRequest().getPostParameters();
		Todo updatedTodo = new Todo(postParameters.getParameterValue(Config.TodoFields.Title.name()).toString(), postParameters.getParameterValue(Config.TodoFields.Description.name()).toString());
		if (!postParameters.getParameterValue(Config.TodoFields.ID.name()).isEmpty()) {
			updatedTodo.setId(postParameters.getParameterValue(Config.TodoFields.ID.name()).toLong());
		}
		updatedTodo.setAssignee(new AssigneeDao().findById(postParameters.getParameterValue(Config.TodoFields.Assignee.name()).toLong()));
		updatedTodo.setBuilding(new BuildingDao().findById(postParameters.getParameterValue(Config.TodoFields.Building.name()).toLong()));
		if (!postParameters.getParameterValue(Config.TodoFields.ID.name()).isEmpty()) {
			todoDao.update(updatedTodo);
		} else {
			todoDao.save(updatedTodo);
		}

	}

}
