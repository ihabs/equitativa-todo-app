package com.equitativa.page.components;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.attributes.AjaxCallListener;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.equitativa.page.EditPage;
import com.equitativa.page.ProjectsPage;
import com.equitativa.persistence.dao.TodoDao;
import com.equitativa.persistence.model.Todo;

/**
 * Panel that houses row-actions
 */
public class ActionPanel extends Panel {

	private static final String DELETE_CONFIRM_MSG = "return confirm(' Are you sure ? ');";
	private static final String REMOVE = "remove";
	private static final String EDIT = "edit";
	private static final long serialVersionUID = 1L;

	public ActionPanel(String id, IModel<Todo> model, EditModal editModal, PageParameters pageParameters) {
		super(id, model);
		
		AjaxLink<Void> editButton = new AjaxLink<Void>(EDIT) {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {

				editModal.setPageCreator(() -> new EditPage(pageParameters, ((Todo) ActionPanel.this.getDefaultModelObject()), editModal));
				editModal.show(target);
			}
		};
		add(editButton);

		AjaxLink<String> removeLink = new AjaxLink<String>(REMOVE) {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Todo todo = (Todo) ActionPanel.this.getDefaultModelObject();
				new TodoDao().delete(todo);
				setResponsePage(ProjectsPage.class, pageParameters);
			}

			@Override
			protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
				//show confirmation message before deleting item
				super.updateAjaxAttributes(attributes);
				AjaxCallListener ajaxCallListener = new AjaxCallListener();
				ajaxCallListener.onPrecondition(DELETE_CONFIRM_MSG);
				attributes.getAjaxCallListeners().add(ajaxCallListener);
			}

			@Override
			public MarkupContainer setDefaultModel(IModel model) {
				return super.setDefaultModel(model);
			}
		};
		add(removeLink);
	}
}
