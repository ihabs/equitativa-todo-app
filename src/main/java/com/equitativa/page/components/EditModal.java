package com.equitativa.page.components;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow.WindowClosedCallback;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.equitativa.page.ProjectsPage;

public class EditModal extends ModalWindow implements WindowClosedCallback {

	private static final long serialVersionUID = 1L;
	
	private PageParameters pageParameters;

	public EditModal(String id,PageParameters pageParameters) {
		super(id);
		this.pageParameters = pageParameters;
	}
	
	@Override
	public void onClose(AjaxRequestTarget target) {
		//reload page to update data table on save
		setResponsePage(ProjectsPage.class, pageParameters);
	}
	

}
