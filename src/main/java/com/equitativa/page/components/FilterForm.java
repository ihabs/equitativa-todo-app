package com.equitativa.page.components;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.equitativa.common.Config;
import com.equitativa.page.ProjectsPage;
import com.equitativa.persistence.dao.AssigneeDao;
import com.equitativa.persistence.dao.BuildingDao;
import com.equitativa.persistence.model.Assignee;
import com.equitativa.persistence.model.Building;

public class FilterForm extends Form<String> {

	private static final long serialVersionUID = 1L;

	public FilterForm(String id, PageParameters pageParameters) {
		super(id);

		IModel<List<Assignee>> assignees = () -> new AssigneeDao().findAll();
		final DropDownChoice<Assignee> assignee = new DropDownChoice<>(Config.Entity.Assignee.name().toLowerCase(), new Model<>(), assignees, new ChoiceRenderer<Assignee>() {

			private static final long serialVersionUID = 1L;

			public Object getDisplayValue(Assignee object) {
				return object.getName();
			};

			public String getIdValue(Assignee object, int index) {
				return String.valueOf(object.getId());
			};

		});

		assignee.setNullValid(true);

		//set selected item on reload
		if (!pageParameters.get(Config.Entity.Assignee.name().toLowerCase()).isNull() && !pageParameters.get(Config.Entity.Assignee.name().toLowerCase()).isEmpty()) {
			List<Assignee> list = assignees.getObject();
			for (Assignee obj : list) {
				if (obj.getId() == pageParameters.get(Config.Entity.Assignee.name().toLowerCase()).toLong()) {
					assignee.setDefaultModelObject(obj);
					break;
				}
			}

		}

		assignee.add(new OnChangeAjaxBehavior() {

			private static final long serialVersionUID = 1L;
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				pageParameters.set(Config.Entity.Assignee.name().toLowerCase(), assignee.getValue());
				setResponsePage(ProjectsPage.class, pageParameters);
			}

		});

		IModel<List<Building>> buildings = () -> new BuildingDao().findAll();

		final DropDownChoice<Building> building = new DropDownChoice<>(Config.Entity.Building.name().toLowerCase(), new Model<>(), buildings, new ChoiceRenderer<Building>() {
			private static final long serialVersionUID = 1L;

			public Object getDisplayValue(Building object) {
				return object.getName();
			};

			public String getIdValue(Building object, int index) {
				return String.valueOf(object.getId());
			};
		});

		building.setNullValid(true);
		//set selected item on reload
		if (!pageParameters.get(Config.Entity.Building.name().toLowerCase()).isNull() && !pageParameters.get(Config.Entity.Building.name().toLowerCase()).isEmpty()) {
			List<Building> list = buildings.getObject();
			for (Building obj : list) {
				if (obj.getId() == pageParameters.get(Config.Entity.Building.name().toLowerCase()).toLong()) {
					building.setDefaultModelObject(obj);
					break;
				}
			}

		}

		building.add(new OnChangeAjaxBehavior() {

			private static final long serialVersionUID = 1L;
			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				pageParameters.set(Config.Entity.Building.name().toLowerCase(), building.getValue());
				setResponsePage(ProjectsPage.class, pageParameters);
			}

		});

		add(assignee);
		add(building);

	}

}
