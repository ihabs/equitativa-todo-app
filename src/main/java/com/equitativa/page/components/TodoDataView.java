package com.equitativa.page.components;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.equitativa.common.Config;
import com.equitativa.persistence.model.Todo;

public class TodoDataView extends DataView<Todo> {

	private static final String ACTIONS_ID = "actions";
	private static final String ODD_ROW_CLASS = "odd";
	private static final String EVEN_ROW_CLASS = "even";
	
	private static final long serialVersionUID = 1L;
	private EditModal editModal;
	private PageParameters pageParameters;

	public TodoDataView(String id, IDataProvider<Todo> dataProvider, EditModal editModal, PageParameters pageParameters) {
		super(id, dataProvider);
		this.editModal = editModal;
		this.pageParameters = pageParameters;
		setItemsPerPage(8L);
	}

	@Override
	protected void populateItem(Item<Todo> item) {
		
		Todo todo = item.getModelObject();
		item.add(new ActionPanel(ACTIONS_ID, item.getModel() ,editModal ,pageParameters));
		item.add(new Label(Config.TodoFields.ID.name(), String.valueOf(todo.getId())));
		item.add(new Label(Config.TodoFields.Title.name(), Model.of(todo.getTitle())));
		item.add(new Label(Config.TodoFields.Description.name(), Model.of(todo.getDescription())));
		item.add(new Label(Config.TodoFields.Assignee.name(), Model.of(todo.getAssignee().getName())));
		item.add(new Label(Config.TodoFields.Building.name(), Model.of(todo.getBuilding().getName())));

		item.add(AttributeModifier.replace("class", new IModel<String>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {
				return (item.getIndex() % 2 == 1) ? EVEN_ROW_CLASS : ODD_ROW_CLASS;
			}
		}));
		
	}

}
