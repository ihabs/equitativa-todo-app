package com.equitativa.data.provider;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.util.string.StringValue;

import com.equitativa.persistence.dao.TodoDao;
import com.equitativa.persistence.model.Todo;

/**
 * 
 * TodoDataProvider used to query the database using the TodoDao.
 *
 */
public class TodoDataProvider implements IDataProvider<Todo> {
	private static final long serialVersionUID = 1L;

	private List<Todo> todoList;

	public TodoDataProvider(StringValue assignee, StringValue building) {
		TodoDao todoDao = new TodoDao();
		if (!assignee.isNull() || !building.isNull()) {
			// find todos by assignee or/and building
			todoList = todoDao.findByParams(assignee.toString(), building.toString());
		} else {
			todoList = todoDao.findAll();
		}
	}

	@Override
	public Iterator<? extends Todo> iterator(long first, long count) {
		return todoList.listIterator((int) first);
	}

	@Override
	public long size() {
		return todoList.size();
	}

	@Override
	public IModel<Todo> model(Todo object) {
		return new DetachableTodoModel(object);
	}

}
