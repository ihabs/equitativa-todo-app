package com.equitativa.data.provider;

import org.apache.wicket.model.LoadableDetachableModel;

import com.equitativa.persistence.model.Todo;

/**
 * 
 * DetachableTodoModel used as a model by the data provider to load the todo
 * entity.
 *
 */
public class DetachableTodoModel extends LoadableDetachableModel<Todo> {

	private static final long serialVersionUID = 1L;

	private Todo todo;

	public DetachableTodoModel(Todo todo) {
		this.todo = todo;
	}

	@Override
	protected Todo load() {
		return todo;
	}

}
