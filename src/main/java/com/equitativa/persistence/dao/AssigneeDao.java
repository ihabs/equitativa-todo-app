package com.equitativa.persistence.dao;

import java.util.List;

import com.equitativa.common.Config;
import com.equitativa.persistence.model.Assignee;
/**
 * 
 * AssigneeDao curd operations for the Assignee table. 
 *
 */
public class AssigneeDao extends Dao<Assignee, Long> {

	private static final String FINDALL_QUERY = "from "+Config.Entity.Assignee.name()+" order by ID";

	public AssigneeDao() {
	}

	public void save(Assignee entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().save(entity);
		closeCurrentSessionwithTransaction();
	}

	public void update(Assignee entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().update(entity);
		closeCurrentSessionwithTransaction();
	}

	public Assignee findById(Long id) {
		openCurrentSession();
		Assignee entity = getCurrentSession().get(Assignee.class, id);
		closeCurrentSession();
		return entity;
	}

	public void delete(Assignee entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().delete(entity);
		closeCurrentSessionwithTransaction();
	}

	public List<Assignee> findAll() {
		openCurrentSession();
		List<Assignee> entityList = getCurrentSession().createQuery(String.format(FINDALL_QUERY, Config.Entity.Assignee.name()), Assignee.class).list();
		closeCurrentSession();
		return entityList;
	}

	public void deleteAll() {
		openCurrentSessionwithTransaction();
		List<Assignee> entityList = findAll();
		for (Assignee entity : entityList) {
			delete(entity);
		}
		closeCurrentSessionwithTransaction();
	}
}
