package com.equitativa.persistence.dao;

import java.util.List;

import com.equitativa.common.Config;
import com.equitativa.persistence.model.Building;

/**
 * 
 * BuildingDao curd operations for the Building table.
 *
 */
public class BuildingDao extends Dao<Building, Long> {

	private static final String FINDALL_QUERY = "from "+Config.Entity.Building.name()+" order by ID";

	public BuildingDao() {
	}

	public void save(Building entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().save(entity);
		closeCurrentSessionwithTransaction();
	}

	public void update(Building entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().update(entity);
		closeCurrentSessionwithTransaction();
	}

	public Building findById(Long id) {
		openCurrentSession();
		Building entity = getCurrentSession().get(Building.class, id);
		closeCurrentSession();
		return entity;
	}

	public void delete(Building entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().delete(entity);
		closeCurrentSessionwithTransaction();
	}

	public List<Building> findAll() {
		openCurrentSession();
		List<Building> entityList = getCurrentSession().createQuery(String.format(FINDALL_QUERY, Config.Entity.Building.name()), Building.class).list();
		closeCurrentSession();
		return entityList;
	}

	public void deleteAll() {
		openCurrentSessionwithTransaction();
		List<Building> entityList = findAll();
		for (Building entity : entityList) {
			delete(entity);
		}
		closeCurrentSessionwithTransaction();
	}
}
