package com.equitativa.persistence.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.equitativa.common.Config;
import com.equitativa.persistence.model.Assignee;
import com.equitativa.persistence.model.Building;
import com.equitativa.persistence.model.Todo;
/**
 * 
 *Dao abstract class declare the common crud operations. 
 *
 * @param <T>
 * @param <Id>
 */
public abstract class Dao<T, Id extends Serializable> {

	public static final String FINDALL_QUERY = "from %s order by ID";

	private static SessionFactory sessionFactory;
	
	private Session currentSession;

	private Transaction currentTransaction;

	public abstract void save(T entity);

	public abstract void update(T entity);

	public abstract T findById(Id id);

	public abstract void delete(T entity);

	public abstract List<T> findAll();

	public abstract void deleteAll();

	protected SessionFactory getSessionFactory() {
		// create single session factory to be used in the subclasses
		if (sessionFactory == null) {
			Configuration configuration = new Configuration().configure();
			configuration.addAnnotatedClass(Todo.class);
			configuration.addAnnotatedClass(Building.class);
			configuration.addAnnotatedClass(Assignee.class);
			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
			sessionFactory = configuration.buildSessionFactory(builder.build());
		}
		return sessionFactory;
	}
	
	protected Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	protected Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	protected void closeCurrentSession() {
		currentSession.close();
	}

	protected void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	protected Session getCurrentSession() {
		return currentSession;
	}

}
