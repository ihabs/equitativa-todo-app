package com.equitativa.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Order;
import org.hibernate.query.criteria.internal.OrderImpl;

import com.equitativa.common.Config;
import com.equitativa.persistence.model.Todo;
/**
 * 
 * TodoDao curd operations for the todo table. 
 *
 */
public class TodoDao extends Dao<Todo, Long> {


	public TodoDao() {
	}

	public void save(Todo entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().save(entity);
		closeCurrentSessionwithTransaction();
	}

	public void update(Todo entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().update(entity);
		closeCurrentSessionwithTransaction();
	}

	public Todo findById(Long id) {
		openCurrentSession();
		Todo entity = (Todo) getCurrentSession().get(Todo.class, id);
		closeCurrentSession();
		return entity;
	}

	public void delete(Todo entity) {
		openCurrentSessionwithTransaction();
		getCurrentSession().delete(entity);
		closeCurrentSessionwithTransaction();
	}

	public List<Todo> findAll() {
		openCurrentSession();
		List<Todo> entityList = getCurrentSession().createQuery(String.format(FINDALL_QUERY, Config.Entity.Todo.name()), Todo.class).list();
		closeCurrentSession();
		return entityList;
	}

	@SuppressWarnings("unchecked")
	public List<Todo> findByParams(String assignee, String building) {
		openCurrentSession();

		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Todo> query = builder.createQuery(Todo.class);
		Root<Todo> root = query.from(Todo.class);
		query.select(root);

		List<Predicate> predicates = new ArrayList<>();

		if (assignee != null && !assignee.isEmpty() && building != null && !building.isEmpty()) {
			predicates.add(builder.equal(root.get(Config.Entity.Assignee.name().toLowerCase()), Long.valueOf(assignee)));
			predicates.add(builder.equal(root.get(Config.Entity.Building.name().toLowerCase()), Long.valueOf(building)));
		} else if (assignee != null && !assignee.isEmpty()) {
			predicates.add(builder.equal(root.get(Config.Entity.Assignee.name().toLowerCase()), Long.valueOf(assignee)));
		} else if (building != null && !building.isEmpty()) {
			predicates.add(builder.equal(root.get(Config.Entity.Building.name().toLowerCase()), Long.valueOf(building)));
		}
		Predicate[] predicatesArray = new Predicate[predicates.size()];
		query.where(predicates.toArray(predicatesArray));
		query.orderBy(builder.asc(root.get(Config.TodoFields.ID.name().toLowerCase())));
		Query q = getCurrentSession().createQuery(query);
		List<Todo> todos = q.getResultList();

		closeCurrentSession();
		return todos;
	}

	public void deleteAll() {
		openCurrentSessionwithTransaction();
		List<Todo> entityList = findAll();
		for (Todo entity : entityList) {
			delete(entity);
		}
		closeCurrentSessionwithTransaction();
	}
}
