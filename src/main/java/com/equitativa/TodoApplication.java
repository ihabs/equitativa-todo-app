package com.equitativa;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.equitativa.page.HomePage;
import com.equitativa.page.ProjectsPage;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 * 
 * @see com.equitativa.Start#main(String[])
 */
public class TodoApplication extends WebApplication {
	private static final Logger logger = LoggerFactory.getLogger(TodoApplication.class);

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage() {
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		super.init();

		mountPage("projects", ProjectsPage.class);

		logger.info("TodoApplication started");
	}
}
